const mongoose = require('mongoose');
let deviceSchema = new mongoose.Schema({
        model:{
            type:String,
            required:true,
        },
        deviceType:{
            type:String,
            required:true
        },
        deviceID:{
            type:String,
            unique:true,
            required:true
        }
    });

deviceSchema.method.someMethod = function(){
    console.log('inside a model\'s method')
}    

let Device = mongoose.model('Device',deviceSchema);
module.exports = Device;    