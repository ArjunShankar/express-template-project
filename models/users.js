const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const CONFIG = require('../config/config');

let userSchema = new mongoose.Schema({      //todo : use custom validators within schema. check length of username/password etc.
    name: {
        type:String,
        required: true,
        default: '' 
    },
    email: {
        type: String,
        unique: true, 
        trim: true
    },
    password: {
        type: String,
        default: ''
    },
    salt: {
        type: String
    },
    activated: {                            //todo : can use this to activate user. No real implementation for now
        type: Boolean,
        default: true
    },
    roles:{
        type:Array
    },
    token: {type: Object},
    tokenExpiryTime: {type: Date},

    createdAt: {type: Number, required: true, default: Date.now}
});


userSchema.method('hashPassword', (salt, password)=> {
    return bcrypt.hashSync(password, salt).toString('base64');

});
userSchema.method('getSalt', ()=> {
    if (this.salt) {
        return this.salt;
    } else {
        return bcrypt.genSaltSync(CONFIG.saltGenerationRounds);
    }
});


userSchema.method('authenticate', (password, passwordToBeVerified, salt)=> {
    return bcrypt.compareSync(password, passwordToBeVerified);
});

module.exports = mongoose.model('users', userSchema);