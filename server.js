const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const cors = require('cors');
const async = require('async');
const chalk = require('chalk');
const cluster = require('cluster');
const routes = require('./routes');

const CONFIG = require('./config/config');

const authMiddleware = require('./middlewares/authenticateRequests')

let NUMBER_OF_CPUs = 2;//require('os').cpus().length;   // change this as per convenience. TODO use a flag in CONFIG to control this.

/**
 *  mongo config
 */
let mongoDBUrl = 'mongodb://' + CONFIG.mongoHost+':'+CONFIG.mongoPort + '/' + CONFIG.mongoSchema;
mongoose.Promise = global.Promise;

let connectWithRetry = () =>{
    mongoose.connect(mongoDBUrl,null,function(err){
        if(err){
            console.log(" MongoDB connection error. Please make sure MongoDB is running. Will retry shortly...");
            setTimeout(connectWithRetry,2000)
        }
        else{
            console.log("MongoDB connection established");
            setupServer();    // only after mongo connection has been established...proceed with creating the server
        }
    });
}

connectWithRetry();     // this will keep on trying to get the mongo connection until success.

let setupServer = () => {
    if(cluster.isMaster){
        for(var i=0;i<NUMBER_OF_CPUs;i++){
            let newWorker = cluster.fork();
            console.log("starting a new worker with pid::" + newWorker.process.pid);
        }

        cluster.on('exit', (worker) => {
            console.error('worker ' + worker.process.pid + ' died');
        let newWorker = cluster.fork();
        console.log("starting a new worker with pid::" + newWorker.process.pid);
    });
    }
    else{
        var app = express();
        app.set('port', CONFIG.port || 3000);
        app.use(cors());
        app.use(bodyParser.urlencoded({extended:true}));
        app.use(bodyParser.json());
        app.listen(app.get('port'), () => {
            console.log("App is running at http://localhost: %d in %s mode", app.get('port'), app.get('env'));
            console.log('Press CTRL-C to stop\n');
        });
        app.use('/api/*',authMiddleware);
        app.use(routes);
        process.on('SIGINT', function() {
            mongoose.connection.close(function () {
                console.log('Mongoose default connection disconnected through app termination');
                process.exit(0);
            });
        });
        module.exports = app;
    }
}



