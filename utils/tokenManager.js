"use strict";
const jwt = require('jwt-simple');
let secret = 'EveryoneSaysThisShouldNotBeHere'; //TODO move this .. but where ??

class TokenManager {
    generateToken(user) {
        let token = jwt.encode({    
            email: user.email,
            roles:user.roles,
            timestamp: new Date().getTime()
        }, secret);
        return token;
    }

    decodeToken(token) {
        let user = jwt.decode(token, secret);
        return user;
    }

    expiresIn(numHrs) {
        var dateObj = Date.now();
        return dateObj + (numHrs * 3600000);
    }

}
module.exports = new TokenManager();