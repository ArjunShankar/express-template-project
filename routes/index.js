var express = require('express');
var router = express.Router();

var deviceController = require('../controllers/devices.controller');
var userController = require('../controllers/users.controller');

router.use(function timeLog (req, res, next) {
  console.log('URL Called: %s | time: ',req.originalUrl, Date.now());
  var headerStr = "Content-Type,";
        for (var headerKey in req.headers) {
            if (headerKey == 'access-control-request-headers') {
                headerStr = headerStr + req.header(headerKey);
            } else {
                headerStr = headerStr + headerKey;
            }
            headerStr = headerStr + ",";
        }
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
  res.header("Access-Control-Expose-Headers", "Content-Disposition");
  res.header("Access-Control-Allow-Headers", headerStr);
  next();
})

router.get('/api/devices', deviceController.getDeviceData);
router.get('/api/devices/:id', deviceController.getDeviceDataByID);
router.post('/api/devices', deviceController.addDevice);
router.put('/api/devices/:id', deviceController.updateDevice);
router.delete('/api/devices/:id', deviceController.deleteDevice);
//TODO.. patch operation. but this will put the onus of sending the delta on the UI.seems inefficient


router.post('/registerUser', userController.registerUser);
router.post('/login', userController.login);
router.post('/validateSession', userController.validateSession);

router.get('/test', function(err,res){
    res.status(200).send({msg:'Express sample app is up.This is an unsecured test api.'});
});

module.exports = router;
