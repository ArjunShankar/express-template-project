'use strict';
let config = {
    port:8081,
    mongoHost:'localhost',
    mongoPort:27017,
    mongoSchema:'testDB',
    saltGenerationRounds:5,
    tokenExpiryTime:1     //in hours
}

module.exports = config;