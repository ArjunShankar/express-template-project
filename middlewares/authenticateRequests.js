let User = require('../models/users');
let tokenManager = require('../utils/tokenManager');
let CONFIG = require('../config/config');
let HTTP_CONSTANTS = require('../config/httpStatusConstants');

function middlewares(req,res,next){
  console.log('in auth handler');
  if(req.method != 'OPTIONS'){
    if(process.env.unsecured != null && process.env.unsecured === 'true'){
      next();
    }
    else{
      let header = req.header('Authorization');
      User.findOne({token:header},function(mongoErr,user){
        if(mongoErr){
            return res.status(HTTP_CONSTANTS.ERROR).send({message: 'Random DB Error'});
        }
        else if(!user){
            res.status(HTTP_CONSTANTS.UNAUTHORIZED).send({message: 'User not authenticated.'});
        }
        else{
          if(user.tokenExpiryTime > Date.now()){
            //update token expiry
            user.tokenExpiryTime = tokenManager.expiresIn(CONFIG.tokenExpiryTime)
            user.save((tokenErr,tokenRes) => {
                        if (tokenErr) {
                            return res.status(HTTP_CONSTANTS.ERROR).send({message: 'DB Error in updating token'
                          });
                        }
                        else if(tokenRes){
                           next();
                          }                      
                        });

          }
          else{
            return res.status(HTTP_CONSTANTS.UNAUTHORIZED).send({message: 'Session expired'});
          }
        }
      })

    }
  }

}



module.exports = middlewares;