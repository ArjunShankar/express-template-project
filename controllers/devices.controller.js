let DeviceModel = require('../models/devices');
let HTTP_CONSTANTS = require('../config/httpStatusConstants');

module.exports.getDeviceData = (request,response) => {
    var query = request.query || {};
    DeviceModel.find(query,function(err,res){
        if(err){
            response.status(HTTP_CONSTANTS.ERROR).json(err);
        }
        else if(res){
            response.status(HTTP_CONSTANTS.SUCCESS).json(res);
        }

    })
   
}

module.exports.getDeviceDataByID = (request,response) => {
    DeviceModel.findById(request.params.id,function(err,res){
        if(err){
            response.status(HTTP_CONSTANTS.ERROR).json(err);
        }
        else if(res){
            response.status(HTTP_CONSTANTS.SUCCESS).json(res);
        }

    })
}

module.exports.addDevice = (request,response) => {
    var device = new DeviceModel(request.body); //TODO deally we should validate the input before passing it on to mongo. Prevent bad data and injection attacks.
    device.save(function(err,res){
        if(err){
            return response.status(HTTP_CONSTANTS.BAD_REQUEST).send({
                message:err
            });
        }
        else if(res){
            return response.status(HTTP_CONSTANTS.CREATED).send({
                message:'Row created.'
            })
        }

    })
}

module.exports.updateDevice = (request,response) => {
    var query = request.params.id;
    var device = request.body; //ideally we should validate the input before passing it on to mongo. Prevent bad data and injection attacks.
    DeviceModel.findOneAndUpdate({_id:query},device,function(err,res){
        if(err){
            return response.status(HTTP_CONSTANTS.BAD_REQUEST).send({
                message:err
            });
        }
        else if(res){
            return response.status(HTTP_CONSTANTS.CREATED).send({
                message:'Row updated.'
            })
        }

    })
}

module.exports.deleteDevice = (request,response) => {
    DeviceModel.findById(request.params.id,function(err,res){       // TODO findByID is being used twice. Can be moved into a  middleware. but should not affect update operation
        if(err){
            response.status(HTTP_CONSTANTS.ERROR).json(err);
        }
        else if(res){
            res.remove(function (err,res) {
                if(err){
                    res.status(HTTP_CONSTANTS.ERROR).send({
                        message:err
                    })
                }
                else{
                    res.status(HTTP_CONSTANTS.SUCCESS).send({
                        message:"Row Deleted"
                    })
                }
            })
            response.status(HTTP_CONSTANTS.SUCCESS).json(res);
        }

    })
}
