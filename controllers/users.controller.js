let userModel = require('../models/users');
let HTTP_CONSTANTS = require('../config/httpStatusConstants');
let tokenManager = require('../utils/tokenManager');
let CONFIG = require('../config/config');

module.exports.registerUser = (request,response) =>{

 if (!request.body.password || !request.body.email) {
        return response.status(500).send({
            message: 'Details missing.'
        });
    }
// do any other checks required before we start creating a user. For eg. check emailID against a domain whitelist

    let userObj = new userModel({
        name: request.body.name,
        email: request.body.email,
        //
    });
    userObj.salt = userObj.getSalt();
    userObj.password = userObj.hashPassword(userObj.salt,request.body.password);

    userObj.save(function(err,res){
        if(err){
            return response.status(HTTP_CONSTANTS.BAD_REQUEST).send({
                message:err
            });
        }
        else if(res){
            return response.status(HTTP_CONSTANTS.SUCCESS).send({
                message:'Account created'
            })
        }

    })
}


module.exports.login = (request, response) => {

    var resp = {
            failure: {"message": "Some Error occurred."},
            invalid: {"message": "Invalid username or password, please correct and try again"},
            notFound: {"message": "User not found"},
            inactive: {
                "message": "This user is currently inactive."
            }
    };

    if(request.body.email && request.body.password){
        userModel.findOne({email:request.body.email},function(err,user){
            if(err || !user){
                return response.status(HTTP_CONSTANTS.UNAUTHORIZED).send(resp.notFound);
            }
            else if(user.activated == false){
                return response.status(HTTP_CONSTANTS.UNAUTHORIZED).send(resp.inactive);
            } 
            else{
                let authenticated = user.authenticate(request.body.password,user.password,user.salt);
                if(authenticated){
                    if(user.token && user.tokenExpiryTime > Date.now()){    //user already has a non-expired token
                        /*
                        let temp = tokenManager.decodeToken(user.token)
                        console.log(temp);
                        */                       
                        return response.status(HTTP_CONSTANTS.SUCCESS).send(getUserObj(user));
                    }
                    else{   // user has no token or token expired
                        user.token = tokenManager.generateToken(user);
                        user.tokenExpiryTime = tokenManager.expiresIn(CONFIG.tokenExpiryTime);
                        user.save((tokenErr,tokenRes) => {
                            if (tokenErr) {
                                return res.status(HTTP_STATUS.ERROR).send({message: 'DB Error in updating token'
                                });
                            }
                            else if(tokenRes){
                                return response.status(HTTP_CONSTANTS.SUCCESS).send(getUserObj(user));
                            }                      
                        });

                    }
                }
                else{
                    return response.status(HTTP_CONSTANTS.UNAUTHORIZED).send(resp.invalid);
                }
            }

        })

    }
    else{
        return response.status(HTTP_CONSTANTS.BAD_REQUEST).send(resp.invalid);
    }
};

module.exports.validateSession =(request,response) =>{
    if(request.body.email && request.body.authToken){
        userModel.findOne({$and:[{email:request.body.email},{token:request.body.authToken}]},function(err,user){
            if(err || !user){
                return response.status(HTTP_CONSTANTS.UNAUTHORIZED).send(resp.notFound);
            }
            else{
                if(user.token && user.tokenExpiryTime > Date.now()){    //user already has a non-expired token  
                    user.tokenExpiryTime = tokenManager.expiresIn(CONFIG.tokenExpiryTime); //update the token expiry time
                    user.save((tokenErr,tokenRes) => {
                            if (tokenErr) {
                                return res.status(HTTP_STATUS.ERROR).send({message: 'DB Error in updating token'
                                });
                            }
                            else if(tokenRes){
                                return response.status(HTTP_CONSTANTS.SUCCESS).send(getUserObj(user));
                            }                      
                        });
                }
                else{
                    return response.status(HTTP_CONSTANTS.UNAUTHORIZED).send(resp.notFound);   
                    }              
            }
        });
    }
    else{
        return response.status(HTTP_CONSTANTS.BAD_REQUEST).send(resp.invalid);
    }
}

let getUserObj = (user) =>{
    let toReturn = {
        name:user.name,
        email:user.email,
        authToken:user.token,
        roles:user.roles
    }
    return toReturn;
}
